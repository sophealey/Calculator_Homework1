//
//  ViewController.swift
//  Calculator
//
//  Created by Soeng Saravit on 10/25/17.
//  Copyright © 2017 Soeng Saravit. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Create outlet of button
    @IBOutlet weak var labelResult: UILabel!
    @IBOutlet weak var lableScreen: UILabel!
    @IBOutlet weak var buttonC: customButton!
    @IBOutlet weak var buttonModule: customButton!
    @IBOutlet weak var buttonToggle: customButton!
    @IBOutlet weak var buttonDevide: customButton!
    @IBOutlet weak var buttonMultiply: customButton!
    @IBOutlet weak var buttonMinus: customButton!
    @IBOutlet weak var buttonPlus: customButton!
    @IBOutlet weak var buttonEqual: customButton!
    @IBOutlet weak var buttonDot: customButton!
    @IBOutlet weak var button0: customButton!
    @IBOutlet weak var button1: customButton!
    @IBOutlet weak var button2: customButton!
    @IBOutlet weak var button3: customButton!
    @IBOutlet weak var button4: customButton!
    @IBOutlet weak var button5: customButton!
    @IBOutlet weak var button6: customButton!
    @IBOutlet weak var button7: customButton!
    @IBOutlet weak var button8: customButton!
    @IBOutlet weak var button9: customButton!
    
    //declear global variable
    var strScreen:String = ""
    var numberOnScreen:Double = 0
    var previousNum:Double = 0
    var performingMath:Bool = false  //when we click on math's sign(+,-,x,/)
    var sign:String = ""
    var strResult:NSString = ""
    var shownResult:Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button0.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        button1.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        button2.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        button3.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        button4.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        button5.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        button6.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        button7.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        button8.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        button9.addTarget(self, action: #selector(ViewController.btnNumPressAction(_:)), for: UIControlEvents.touchUpInside)
        buttonC.addTarget(self, action: #selector(ViewController.btnCPressAction(_:)), for: UIControlEvents.touchUpInside)
         buttonToggle.addTarget(self, action: #selector(ViewController.btnTogglePressAction(_:)), for: UIControlEvents.touchUpInside)
         buttonModule.addTarget(self, action: #selector(ViewController.btnSignPressAction(_:)), for: UIControlEvents.touchUpInside)
         buttonDevide.addTarget(self, action: #selector(ViewController.btnSignPressAction(_:)), for: UIControlEvents.touchUpInside)
         buttonMultiply.addTarget(self, action: #selector(ViewController.btnSignPressAction(_:)), for: UIControlEvents.touchUpInside)
         buttonMinus.addTarget(self, action: #selector(ViewController.btnSignPressAction(_:)), for: UIControlEvents.touchUpInside)
        buttonPlus.addTarget(self, action: #selector(ViewController.btnSignPressAction(_:)), for: UIControlEvents.touchUpInside)
        buttonEqual.addTarget(self, action: #selector(ViewController.btnEqualPressAction(_:)), for: UIControlEvents.touchUpInside)
        buttonDot.addTarget(self, action: #selector(ViewController.btnDotPressAction(_:)), for: UIControlEvents.touchUpInside)
    }
    
    @objc func btnNumPressAction(_ sender: UIButton)  {
        if(lableScreen.text == "0"){
            lableScreen.text = ""
            labelResult.text = ""
        }
        if(shownResult){
            labelResult.text = ""
            lableScreen.text = ""
            numberOnScreen = 0
            previousNum = 0
            shownResult=false
        }
        if(performingMath==true){
            lableScreen.text = (sender.titleLabel?.text)!
           labelResult.text = labelResult.text! + (sender.titleLabel?.text)!
            numberOnScreen = Double(lableScreen.text!)!
            performingMath=false
        }else{
            
            lableScreen.text = lableScreen.text! + (sender.titleLabel?.text)!
            labelResult.text = labelResult.text! + (sender.titleLabel?.text)!
            numberOnScreen = Double(lableScreen.text!)!
        }
    }
    
    @objc func btnSignPressAction(_ sender: UIButton)  {
        shownResult=false
        if(lableScreen.text != ""){

            previousNum = Double(lableScreen.text!)!
            
            if(performingMath != true){
                labelResult.text = labelResult.text! + (sender.titleLabel?.text)!
                sign = (sender.titleLabel?.text)!
                performingMath = true
            }else{
                var strResult:String = labelResult.text!
                strResult.remove(at: strResult.index(before: strResult.endIndex))
                labelResult.text = strResult + (sender.titleLabel?.text)!
                sign = (sender.titleLabel?.text)!
                performingMath = true
            }
        }
    }
    @objc func btnEqualPressAction(_ sender: UIButton)  {
        switch sign {
        case "÷":
            if (String(previousNum / numberOnScreen).hasSuffix(".0")){
                lableScreen.text = String(format:"%g",(previousNum / numberOnScreen))
            }else{
                lableScreen.text = String(previousNum / numberOnScreen)
            }
        case "×":
            if (String(previousNum * numberOnScreen).hasSuffix(".0")){
                lableScreen.text = String(format:"%g",(previousNum * numberOnScreen))
            }else{
                lableScreen.text = String(previousNum * numberOnScreen)
            }
        case "-":
            if (String(previousNum - numberOnScreen).hasSuffix(".0")){
                lableScreen.text = String(format:"%g",(previousNum - numberOnScreen))
            }else{
                lableScreen.text = String(previousNum - numberOnScreen)
            }
        case "+":
            if (String(previousNum + numberOnScreen).hasSuffix(".0")){
                lableScreen.text = String(format:"%g",(previousNum + numberOnScreen))
            }else{
                lableScreen.text = String(previousNum + numberOnScreen)
            }
            
        case "%":
            if (String(previousNum.truncatingRemainder(dividingBy: numberOnScreen)).hasSuffix(".0")){
                lableScreen.text = String(format:"%g",(previousNum.truncatingRemainder(dividingBy: numberOnScreen)))
            }else{
                lableScreen.text = String(previousNum.truncatingRemainder(dividingBy: numberOnScreen))
            }
        default:
            //lableScreen.text = String(numberOnScreen)
            if ((String(numberOnScreen)).hasSuffix(".0")){
                lableScreen.text = String(format:"%g",numberOnScreen)
            }else{
                lableScreen.text = String(numberOnScreen)
            }
        }
        shownResult = true
        labelResult.text = lableScreen.text
    }
    @objc func btnDotPressAction(_ sender: UIButton)  {
        if((lableScreen.text?.characters.contains("."))!) != true {
            lableScreen.text = lableScreen.text! + (sender.titleLabel?.text)!
            labelResult.text = labelResult.text! + (sender.titleLabel?.text)!
        }
        
    }
    @objc func btnTogglePressAction(_ sender: UIButton)  {
        if(lableScreen.text != "" && labelResult.text != ""){
            if(String(-Double(lableScreen.text!)!).contains(".0")){
                lableScreen.text = String(format:"%g",-Double(lableScreen.text!)!)
                labelResult.text = String(format:"%g",-Double(labelResult.text!)!)
            }else{
                lableScreen.text = String(-Double(lableScreen.text!)!)
                labelResult.text = String(-Double(labelResult.text!)!)
            }
        }
    }
    @objc func btnCPressAction(_ sender: UIButton)  {
        strScreen = ""
        numberOnScreen = 0
        previousNum = 0
        performingMath = false  //when we click on math's sign(+,-,x,/)
        sign = ""
        strResult = ""
        lableScreen.text = "0"
        labelResult.text = "0"
    }
}

@IBDesignable class customButton: UIButton {
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0.0 {
        didSet {
           self.layer.cornerRadius = self.cornerRadius
        }
    }
}
